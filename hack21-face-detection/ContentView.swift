//
//  ContentView.swift
//  hack21-face-detection
//
//  Created by Michaud Reyna on 12/15/21.
//

import SwiftUI
import Alamofire
import Resolver



struct ContentView: View {
    @ObservedObject private var viewModel = ContentViewModel()
    
    var body: some View {
        NavigationView {
            ZStack(alignment: .center) {
                Rectangle()
                  .fill(Color.teal)
                  .edgesIgnoringSafeArea(.all)
                PinPadView(showInfoModalView: $viewModel.showInfoModalView, pinStr: $viewModel.pinStr)
                    .sheet(isPresented: $viewModel.showInfoModalView) {ModalView()}
            }
            .navigationBarTitleDisplayMode(.inline)
            .toolbar {
                ToolbarItem(placement: .navigationBarLeading) {
                    Text("RevelUp").foregroundColor(.white)
                }
                ToolbarItem(placement: .principal) {
                    Text(viewModel.pinStr)
                        .frame(width: 100).foregroundColor(.white)
                }
                ToolbarItem(placement: .navigationBarTrailing) {
                    HStack{
                        Text("Refresh").foregroundColor(.white)
                        Spacer().frame(width: 75)
                    }
                }
            }
        }.navigationViewStyle(StackNavigationViewStyle())
    }
}
struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
                .previewInterfaceOrientation(.landscapeRight)
    }
}

extension UIScreen{
   static let screenWidth = UIScreen.main.bounds.size.width
   static let screenHeight = UIScreen.main.bounds.size.height
   static let screenSize = UIScreen.main.bounds.size
}
