//
//  hack21_face_detectionApp.swift
//  hack21-face-detection
//
//  Created by Michaud Reyna on 12/15/21.
//

import SwiftUI

@main
struct hack21_face_detectionApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
