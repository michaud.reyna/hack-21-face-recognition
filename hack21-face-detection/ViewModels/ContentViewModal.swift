//
//  ContentViewModal.swift
//  hack21-face-detection
//
//  Created by Michaud Reyna on 12/17/21.
//

import SwiftUI

extension ContentView {
    class ContentViewModel: ObservableObject{
        @Published var show: Bool = false
        @Published var showInfoModalView: Bool = false
        @Published var pinStr: String = "Enter Pin"
        var service: FacialIdentification
        
        init() {
            service = Kairos()
        }
    }
}

//Network Actions
private extension ContentView.ContentViewModel {
    func onClockIn() {
        let image: String = "http://media.kairos.com/kairos-elizabeth.jpg"
        let id: String = "Elizabeth"

        let block: (Bool) -> Void = { isVerified in
            print("Verification: \(isVerified)")
        }

        service.verifyUser(image: image, id: id, closure: block)
    }

    func Enroll() {
        let image: String = "http://media.kairos.com/kairos-elizabeth.jpg"
        let id: String = "Elizabeth"
        service.enrollUser(image: image, id: id) { isEnrolled in
            print("Enrolled: \(isEnrolled)")
        }
    }
}
