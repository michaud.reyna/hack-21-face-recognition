//
//  KairosModal.swift
//  hack21-face-detection
//
//  Created by Michaud Reyna on 12/17/21.
//

import SwiftUI

struct ModalView: View {
    @State var showCaptureImageView: Bool = false
    @State var image: Image? = nil
    
    var body: some View {
        ZStack {
          VStack {
            Button(action: {
              self.showCaptureImageView.toggle()
            }) {
              Text("Choose photos")
            }
            image?.resizable()
              .frame(width: 250, height: 200)
              .clipShape(Circle())
              .overlay(Circle().stroke(Color.white, lineWidth: 4))
              .shadow(radius: 10)
            if (showCaptureImageView) {
                CaptureImageView(isShown: $showCaptureImageView, image: $image)
            }
          }
        }
    }
}
