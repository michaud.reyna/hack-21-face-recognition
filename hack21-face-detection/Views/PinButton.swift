//
//  PinButton.swift
//  hack21-face-detection
//
//  Created by Michaud Reyna on 12/16/21.
//

import SwiftUI

public struct PinButton : View {
    private var title : String
    private var action: (() -> Void)?
    private var color: Color
    let fontSize : CGFloat
    
    public init(title: String, color: Color, action: (()->Void)?){
        self.title = title
        self.color = color
        self.action = action
        
        if title == "Login" || title == "Clock In" || title == "Clock Out" {
            fontSize = 20
        }else {
            fontSize = 30
        }
        
    }
    public var body: some View {
        HStack {
            Button(title, action: {
                if let performAction = action {performAction()}
            })
            .font(.system(size: fontSize))
            .frame(width: 65, height: 65)
            .background(color)
            .cornerRadius(30)
        }
    }
}
