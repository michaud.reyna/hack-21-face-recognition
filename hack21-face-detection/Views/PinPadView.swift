//
//  PinPadView.swift
//  hack21-face-detection
//
//  Created by Michaud Reyna on 12/16/21.
//

import SwiftUI

struct PinPadView: View {
    @Binding private var showInfoModalView: Bool
    @Binding private var pinn: String
    private var gridItemLayout = [GridItem(.flexible()), GridItem(.flexible()), GridItem(.flexible())]
    private var colors: [Color] = [.yellow, .purple, .green]
    
    public init (showInfoModalView: Binding<Bool>, pinStr: Binding<String>) {
        self._showInfoModalView = showInfoModalView
        self._pinn = pinStr
    }
   
    var body: some View {
        VStack{
            Spacer()
            ScrollView {
              LazyVGrid(columns: gridItemLayout, spacing: 20) {
                  ForEach((1...9), id: \.self) {
                      PinButton(title: "\($0)", color: colors[$0 % colors.count],action: nil)
                  }
                  PinButton(title: "Clock In", color: Color.purple, action:
                                { self.showInfoModalView.toggle() })
                  PinButton(title: "0", color: Color.green, action:{})
                  PinButton(title: "Login", color: Color.purple, action:{})
              }
            }.frame(width: 250, height: UIScreen.screenHeight/2)
            Spacer()
        }
    }
}

struct PinPadView_Previews: PreviewProvider {
    @State private var pinStr : String = "Enter Pin"
    static var previews: some View {
        PinPadView(showInfoModalView: .constant(false), pinStr: .constant(""))
    }
}
