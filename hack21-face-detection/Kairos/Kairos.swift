import Alamofire

struct Transaction: Decodable {
    let confidence: Float
    let face_id: String
    let status: String
}

struct Test: Decodable {
    let transaction: Transaction
}

struct VerifyResponse: Decodable {
    var images: [Test] // "images: []
}

class Kairos: FacialIdentification {
    let url = "https://api.kairos.com"
    let minimalConfidence: Float = 0.30
    let headers: HTTPHeaders = [
        "app_id": "4cc460b9",
        "app_key": "253f545e36c03acba7c159a4aaff701b",
        "Content-Type": "application/json"
    ]

    func verifyUser(image: String, id: String, closure: @escaping (Bool) -> Void) {
        let parameters: [String: String] = [
            "image": image,
            "subject_id": id,
            "gallery_name": "faceID"
        ]

        let request = AF.request(url + "/verify", method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: headers)
        request.responseDecodable(of: VerifyResponse.self) { (response) in
            guard let response = response.value else {
                print("Unable to parse response. Please try again.")
                closure(false)
                return
            }

            if (response.images[0].transaction.confidence < self.minimalConfidence) {
                print("Face Not recognized. Please try again.")
                closure(false)
            }

            print("Face ID passed.")
            closure(true)
        }
    }

    func enrollUser(image: String, id: String, closure: @escaping (Bool) -> Void) {
        let parameters: [String: String] = [
            "image": image,
            "subject_id": id,
            "gallery_name": "faceID"
        ]

        let request = AF.request(url + "/enroll", method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: headers)
        request.responseString { response in
            closure(true)
        }
    }
}