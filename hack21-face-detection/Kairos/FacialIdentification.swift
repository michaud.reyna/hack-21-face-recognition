protocol FacialIdentification{
    func verifyUser(image: String, id: String, closure: @escaping (Bool) -> Void)
    func enrollUser(image: String, id: String, closure: @escaping (Bool) -> Void)
}